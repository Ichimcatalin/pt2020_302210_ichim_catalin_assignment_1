
import java.util.*;

public class Polinom  {
	public ArrayList<Monom> polinom = new ArrayList<Monom>();
	
	public Polinom() {
		
	};
	
	public Polinom(ArrayList<Monom> newPolinom)
	{
		this.polinom=newPolinom;
	}

	public void Sort()
	{
		Collections.sort(this.polinom);
	}
	
	public Polinom add(Polinom p)
	{
		Polinom sum= new Polinom();
		Polinom p1=new Polinom();
		Polinom p2=new Polinom();
		if(this.polinom.get(0).getPower()>p.polinom.get(0).getPower())
			{
				p1.polinom=this.polinom;
				p2.polinom=p.polinom;
				int dif=this.polinom.get(0).getPower()-p.polinom.get(0).getPower();
				for(int i=0; i<dif; i++)
					sum.polinom.add(p1.polinom.get(i));
			}
		else
		{
			p2.polinom=this.polinom;
			p1.polinom=p.polinom;
			int dif=p.polinom.get(0).getPower()-this.polinom.get(0).getPower();
			for(int i=0; i<dif; i++)
				sum.polinom.add(p1.polinom.get(i));
		}
		for(Monom i: p1.polinom)
			for(Monom j: p2.polinom)
			{
				if(i.getPower()==j.getPower())
				{
					sum.polinom.add(i.add(j));
				}
				
			}
		return sum;
	}
	
	public Polinom sub(Polinom p)
	{
		Polinom sub= new Polinom();
		Polinom p1=new Polinom();
		Polinom p2=new Polinom();
		if(this.polinom.get(0).getPower()>p.polinom.get(0).getPower())
		{
			p1.polinom=this.polinom;
			p2.polinom=p.polinom;
			int dif=this.polinom.get(0).getPower()-p.polinom.get(0).getPower();
			for(int i=0; i<dif; i++)
				sub.polinom.add(p1.polinom.get(i));
		}
		else
		{
		p2.polinom=this.polinom;
		p1.polinom=p.polinom;
		int dif=p.polinom.get(0).getPower()-this.polinom.get(0).getPower();
		for(int i=0; i<dif; i++)
			sub.polinom.add(p1.polinom.get(i));
		}
		
		for(Monom i: this.polinom)
			for(Monom j: p.polinom)
			{
				if(i.getPower()==j.getPower())
				{
					sub.polinom.add(i.sub(j));
				}
			}
		return sub;
	}
	
	public Polinom mul(Polinom p)
	{
		Polinom mul=new Polinom();
		Polinom rezP=new Polinom();
		
		for(Monom i: this.polinom)
			for(Monom j: p.polinom)
			{
					mul.polinom.add(i.mul(j));
			}
		mul.Sort();
		rezP.polinom.add(mul.polinom.get(0));
		rezP.polinom.add(mul.polinom.get(mul.polinom.size()-1));
		
		for(int i=0; i<mul.polinom.size()-1; i++)
		{
			while(mul.polinom.get(i).getPower()==mul.polinom.get(i+1).getPower())
			{
				Monom rezM=new Monom();
				rezM.setPower(mul.polinom.get(i).getPower());
				rezM.setCoef(mul.polinom.get(i).getCoef()+mul.polinom.get(i+1).getCoef());
				rezP.polinom.add(rezM);
				i++;
			}
			
		}
		rezP.Sort();
		return rezP;
	}
	
	public Polinom deriv()
	{
		Polinom rez=new Polinom();
		for(Monom i: this.polinom)
		{
			Monom rezM=i.deriv();
			rez.polinom.add(rezM);
		}
		
		return rez;
	}
	public Polinom integ()
	{
		Polinom rez=new Polinom();
		for(Monom i: this.polinom)
		{
			Monom rezM=i.integ();
			rez.polinom.add(rezM);
		}
		
		return rez;
	}
	
	
	public String toString()
	{
		String rez=" ";
		for(Monom iMonom: this.polinom)
		{
			
			if(iMonom.getCoef()>0)
			{	
				rez=rez+"+" +iMonom.toString();
			}
			else if(iMonom.getCoef()<0) 
				rez=rez +iMonom.toString();
			else 
				continue;
		}
		return rez;
	}

	
}

