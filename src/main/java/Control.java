import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Control {
	private View ViewC;
	private Model ModelC;

	public Control(View newView, Model newModel)
	{
		this.ViewC=newView;
		this.ModelC=newModel;
		this.ViewC.addListener(new addListener());
		this.ViewC.subListener(new subListener());
		this.ViewC.mulListener(new mulListener());
		this.ViewC.derListener(new derListener());
		this.ViewC.intListener(new intListener());
		this.ViewC.helpListener(new helpListener());
	}
	
	class addListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			Polinom p1;
			Polinom p2;
			Polinom rez;
			p1=ViewC.getPolinom1();
			p2=ViewC.getPolinom2();
			rez=ModelC.add(p1, p2);
			ViewC.printRez(rez);
			
		}
	}
	
	class subListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			Polinom p1;
			Polinom p2;
			Polinom rez;
			p1=ViewC.getPolinom1();
			p2=ViewC.getPolinom2();
			rez=ModelC.sub(p1, p2);
			ViewC.printRez(rez);
			
		}
	}
	class mulListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			Polinom p1;
			Polinom p2;
			Polinom rez;
			p1=ViewC.getPolinom1();
			p2=ViewC.getPolinom2();
			rez=ModelC.mul(p1, p2);
			ViewC.printRez(rez);
			
		}
	}
	class derListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			Polinom p1;
			Polinom rez;
			p1=ViewC.getPolinom1();
			rez=ModelC.deriv(p1);
			ViewC.printRez(rez);
			
		}
	}
	class intListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			Polinom p1;
			Polinom rez;
			p1=ViewC.getPolinom1();
			rez=ModelC.integ(p1);
			ViewC.printRez(rez);
			
		}
	}
	class helpListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			ViewC.getHelp();
		}
	}
}
