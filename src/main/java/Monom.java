import java.util.*;
import java.lang.Math;

public class Monom implements Comparable{

	private int power;
	private float coef;
	
	public Monom(int power, float coef) {
		super();
		this.power = power;
		this.coef = coef;
	}

	public Monom() {
		super();
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public float getCoef() {
		return coef;
	}

	public void setCoef(float coef) {
		this.coef = coef;
	}
	
	public String toString()
	{	
		if(this.coef==Math.floor(coef))
			return (int)Math.floor(coef)+"*X^"+this.power+ " " ;
		else
			return this.coef+"*X^"+this.power+ " "  ;
	}

	public Monom add(Monom op2)
	{
		
		Monom monom=new Monom();
		if(this.power==op2.power)
		{
			monom.power=this.power;
			monom.coef=this.coef+op2.coef;
		}
		return monom;
	}
	
	public Monom sub(Monom op2)
	{
		Monom rez=new Monom();
		if(this.power==op2.power)
		{
			rez.power=this.power;
			rez.coef=this.coef-op2.coef;
		}
		
		return rez;
	}
	
	public Monom mul(Monom op2)
	{
		Monom rez=new Monom();
		rez.coef=this.coef*op2.coef;
		rez.power=this.power+op2.power;
		return rez;
	}
	
	public int compareTo(Object o) 
	{
		int val=((Monom)o).power;
		return val- this.power;
	}
	public Monom deriv()
	{
		Monom rez=new Monom();
		if(this.power!=0)
		{
			rez.power=this.power-1;
			rez.coef=this.power*this.coef;
		}
		else {
			rez.coef=0;
			rez.power=0;
		}
		return rez;
	}
	public Monom integ()
	{
		
		Monom rez=new Monom();
		rez.power=this.power+1;
		rez.coef=this.coef/(this.power+1);
		return rez;
	}
	
	
}

