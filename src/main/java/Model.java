
public class Model {
	private Polinom rezultat;
	
	public Polinom add(Polinom p1, Polinom p2)
	{
		rezultat=p1.add(p2);
		return rezultat;
	}
	public Polinom sub(Polinom p1, Polinom p2)
	{
		rezultat=p1.sub(p2);
		return rezultat;
	}
	public Polinom mul(Polinom p1, Polinom p2)
	{
		rezultat=p1.mul(p2);
		return rezultat;
	}
	public Polinom deriv(Polinom p1)
	{
		rezultat=p1.deriv();
		return rezultat;
	}
	public Polinom integ(Polinom p1)
	{
		rezultat=p1.integ();
		return rezultat;
	}
}
