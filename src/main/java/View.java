import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

public class View extends JFrame
{
			private JFrame frame= new JFrame("Calculator Polinoame");
			
			JPanel panelBtn = new JPanel();
			JPanel panelRez = new JPanel();
			JPanel panelGr = new JPanel();
			JPanel panelCoef = new JPanel();
			JPanel panel = new JPanel();
			
			JButton jbtadd = new JButton("+");
			JButton jbtsub = new JButton("-");
			JButton jbtmul = new JButton("*");
			JButton jbtdiv = new JButton("/");
			JButton jbtder = new JButton("'");
			JButton jbtint = new JButton("S");
			JButton jbthelp = new JButton("?");
			
			JLabel jlbCoef1 = new JLabel("Coeficenti polinom I", SwingConstants.CENTER);
			JLabel jlbCoef2 = new JLabel("Coeficenti polinom II ", SwingConstants.CENTER);
			JTextField jtfCoef1 = new JTextField("");
			JTextField jtfCoef2 = new JTextField("");
			
			JLabel jlbGr1 = new JLabel("Grad polinom I ");
			JLabel jlbGr2 = new JLabel("Grad polinom II ");
			JTextField jtfGr1 = new JTextField("");
			JTextField jtfGr2 = new JTextField("");
			
			JTextField jtfRez = new JTextField("");
			
			public View()
			{
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(800, 170);
				frame.setVisible(true);
			
			
			//rezultatul 
			 
			jtfRez.setColumns(50);
			panelRez.add(jtfRez);
			
			//grad

			panelGr.setLayout(new GridLayout(4,1));
			panelGr.add(jlbGr1);
			panelGr.add(jtfGr1);
			panelGr.add(jlbGr2);
			panelGr.add(jtfGr2);
			
			//coef

			panelCoef.setLayout(new GridLayout(4, 1));
			panelCoef.add(jlbCoef1);
			panelCoef.add(jtfCoef1);
			panelCoef.add(jlbCoef2);
			panelCoef.add(jtfCoef2);
			
			//butoane pentru operatii

			panelBtn.setLayout(new GridLayout(4,2));
			panelBtn.add(jbtadd);
			jbtadd.setBackground(Color.YELLOW);
			panelBtn.add(jbtsub);
			jbtsub.setBackground(Color.YELLOW);
			panelBtn.add(jbtmul);
			jbtmul.setBackground(Color.green);
			panelBtn.add(jbtdiv);
			jbtdiv.setBackground(Color.green);
			panelBtn.add(jbtder);
			jbtder.setBackground(Color.blue);
			panelBtn.add(jbtint);
			jbtint.setBackground(Color.blue);
			panelBtn.add(jbthelp);
			
			//panel-ul final
			panel.setLayout(new BorderLayout());
			panel.add(panelBtn, BorderLayout.EAST);		
			panel.add(panelRez, BorderLayout.SOUTH);	
			panel.add(panelGr, BorderLayout.WEST);
			panel.add(panelCoef, BorderLayout.CENTER);
			
			frame.setContentPane(panel);
			}
			
			public int getGrad1() 
			{
				String input=jtfGr1.getText();
				if(Integer.parseInt(input)<=0)
					jtfGr1.setBackground(Color.RED);
				return Integer.parseInt(input);
				
			}
			
			public int getGrad2() 
			{
				String input=jtfGr2.getText();
				if(Integer.parseInt(input)<=0)
					jtfGr2.setBackground(Color.RED);
				return Integer.parseInt(input);
			}
			
			public Polinom getPolinom1() 
			{
				int grad=this.getGrad1();
				String input= this.jtfCoef1.getText();
				String[] words=input.split(" ");
				int ipow=0;
				
				ArrayList <Float> coef = new ArrayList<Float>();
				Polinom rezP=new Polinom();
				
				for(String i: words)
				{
					coef.add(Float.parseFloat(i));
				}
				if(grad+1!=coef.size())
				{
					jtfCoef1.setBackground(Color.red);
				}
					
				for(Float i: coef)	
				{		
					
					Monom m= new Monom(grad-ipow,i);
					rezP.polinom.add(m);
					ipow++;
				}
					System.out.println(rezP.toString());
					return rezP;
			}
			
			public Polinom getPolinom2()
			{
				int grad=this.getGrad2();
				String input= this.jtfCoef2.getText();
				String[] words=input.split(" ");
				int ipow=0;
				
				ArrayList <Float> coef = new ArrayList<Float>();
				Polinom rezP=new Polinom();
				
				for(String i: words)
				{
					coef.add(Float.parseFloat(i));
				}
				
				if(grad+1!=coef.size())
				{
					jtfCoef2.setBackground(Color.red);
				}
				
				for(Float i: coef)	
				{		
					
					Monom m= new Monom(grad-ipow,i);
					rezP.polinom.add(m);
					ipow++;
				}
					System.out.println(rezP.toString());
					return rezP;
			}
			
			public void printRez(Polinom p1)
			{
				this.jtfRez.setText(p1.toString());
			}
			public void getHelp()
			{
				JFrame help= new JFrame("Help");
				help.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				help.setSize(520, 200);
				help.setVisible(true);
				JLabel jlbhelp1 = new JLabel("Instructiuni de folosire");
				JLabel jlbhelp2 = new JLabel("1. Puterile introduse trebuie sa fie mai mari sau egale cu 0 ");
				JLabel jlbhelp3 = new JLabel("2. Coeficentii se introduc in ordinea descrescatoare a puterii, separati de un spatiu liber");
				JLabel jlbhelp4 = new JLabel("3. Trebuie specificat daca un coeficent este 0");
				JLabel jlbhelp5 = new JLabel("4. Derivarea si integrarea se face cu valorile polinomului I");
				JLabel jlbhelp6 = new JLabel("5. Impartirea nu este implementata :(");
				help.setLayout(new GridLayout(6, 1));
				help.add(jlbhelp1);
				help.add(jlbhelp2);
				help.add(jlbhelp3);
				help.add(jlbhelp4);
				help.add(jlbhelp5);
				help.add(jlbhelp6);
			}
			
			void addListener(ActionListener listenForAdd)
			{
					jbtadd.addActionListener(listenForAdd);
			}
			void subListener(ActionListener listenForSub)
			{
					jbtsub.addActionListener(listenForSub);
			}
			void mulListener(ActionListener listenForMul)
			{
					jbtmul.addActionListener(listenForMul);
			}
			void derListener(ActionListener listenForDer)
			{
					jbtder.addActionListener(listenForDer);
			}
			void intListener(ActionListener listenForInt)
			{
					jbtint.addActionListener(listenForInt);
			}
			void helpListener(ActionListener listenForHelp)
			{
					jbthelp.addActionListener(listenForHelp);
			}


}