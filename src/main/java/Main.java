import java.util.*;
import javax.swing.*;
import java.awt.*;

public class Main {
	public static void main(String[] args) {

		View mainView = new View();
		Model mainModel = new Model();
		Control mainControl = new Control(mainView, mainModel);
	}
}
